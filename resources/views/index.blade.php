


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <h1 class="my-5">List of All Sliders</h1>
    <table class="table table-bordered">
    <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Title</th>
      <th scope="col">Heading</th>
      <th scope="col">paragraph</th>
      <th scope="col">Image</th>
      <th scope="col">Alt</th>
      <th scope="col">Created By</th>
      <th scope="col">Updated By</th>
      <th scope="col">Created at</th>
      <th scope="col">Updated at</th>
    </tr>
  </thead>
  <tbody>
    @foreach($sliders as $key=>$data)
    <tr>
      <td>{{++$key}}</td>
      <td>{{$data->title}}</td>
      <td>{{$data->heading}}</td>
      <td>{{$data->paragraph}}</td>
      <td>{{$data->image}}</td>
      <td>{{$data->alt}}</td>
      <td>{{$data->created_by}}</td>
      <td>{{$data->updated_by}}</td>
      <td>{{$data->created_at}}</td>
      <td>{{$data->updated_at}}</td>
    </tr>
    @endforeach
  </tbody>
    </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>