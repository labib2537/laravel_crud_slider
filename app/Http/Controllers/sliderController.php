<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class sliderController extends Controller
{
    public function display()
    {
        $sliders = \App\Models\Slider::all();
        return view('index', compact('sliders'));
    }
}
